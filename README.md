# 物联网工业设备管理平台

## 使用前请先阅读Quasar官方文档
[Quasar官网](https://quasar.dev/).

## 请先全局安装Quasar依赖（非常重要！！！）
```
npm install -g @quasar/cli
```

## 请使用Yarn，不要使用NPM安装依赖！（非常重要！！！）

https://yarn.bootcss.com/docs/install/#windows-stable

## 安装依赖

```bash
yarn
```

### 启动开发模式

```bash
quasar dev
```

### 格式化纠错

```bash
yarn run lint
```

### 构建生产环境

```bash
quasar build
```
