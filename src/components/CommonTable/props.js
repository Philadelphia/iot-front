export default {
  flat: {
    type: Boolean,
    default: true
  },
  grid: {
    type: Boolean,
    default: false
  },
  btnSpan: {
    type: [Number, String],
    default: 4
  },
  // 隐藏表格标题
  hideTitle: {
    type: Boolean,
    default: false
  },
  // 隐藏表格底部分页操作栏
  hideBottom: {
    type: Boolean,
    default: false
  },
  // 隐藏对话框底部
  hideDialogBottom: {
    type: Boolean,
    default: false
  },
  // 弹窗表单是否每行两项
  dialogFormHorizontal: {
    type: Boolean,
    default: true
  },
  // 弹窗宽度
  dialogWidth: {
    type: String,
    default: '61.8vw'
  },
  // 行主键，多选时确保该字段在数据中有
  rowKey: {
    type: [Number, String],
    default: 'id'
  },
  // 表格名称
  tableName: {
    type: String,
    default: ''
  },
  // 表格整体加载状态
  loading: {
    type: Boolean,
    default: false
  },
  // 表格列描述
  tableColumnDescription: {
    type: Array,
    default: () => []
  },
  // 表格头部工具栏
  tableHeaderToolForm: {
    type: Array,
    default: () => []
  },
  // 表格头部按钮
  tableHeaderToolBtns: {
    type: Array,
    default: () => []
  },
  // 表格数据
  tableData: {
    type: Array,
    default: () => []
  },
  // 表格是否可选择
  selectable: {
    type: Boolean,
    default: false
  },
  // 表格操作按钮，设置了该值，则默认的编辑与删除按钮不生效
  tableRowOperations: {
    type: Array,
    default: () => []
  },
  // 表格行剩余操作按钮描述，设置该值，则会和默认的编辑，创建合并显示
  tableRowOtherOperations: {
    type: Array,
    default: () => []
  },
  // 是否展示操作列
  showTableRowOptBtn: {
    type: Boolean,
    default: true
  },
  // 应用在表格上的类
  tableClass: {
    type: [String, Array],
    default: ''
  },
  // 分页总条数
  totalRows: {
    type: Number,
    default: 0
  }
}
