export default {
  data () {
    return {
      chartObj: null
    }
  },
  methods: {
    getColor (i) {
      return this.colors[i % this.colors.length]
    },
    destroyChart () {
      if (this.chartObj) {
        this.chartObj.destroy()
      }
    },
    reDrawChart () {

    }
  },
  props: {
    // 显示的字体颜色
    fontColor: {
      type: String,
      default: '#333'
    },
    // 图表标题
    title: {
      type: String,
      default: ''
    },
    // 颜色数组
    colors: {
      type: Array,
      default: () => [
        'rgba(54, 162, 235, 0.5)',
        'rgba(122, 206, 86, 0.5)',
        'rgba(255, 99, 132, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(153, 102, 255, 0.5)',
        'rgba(255, 159, 64, 0.5)'
      ]
    },
    // 图表数据
    chartData: {
      type: Array,
      default: () => []
    },
    // 图表高度
    chartHeight: {
      type: String,
      default: '300px'
    },
    // 标签
    labels: {
      type: Array,
      default: () => []
    }
  },
  watch: {
    chartData (v) {
      this.reDrawChart()
    }
  }
}
