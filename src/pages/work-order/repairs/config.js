export default {
  title: '报修工单',
  fieldArr: [
    ['name', '客户名称'],
    ['address', '客户地址'],
    ['tel', '固定电话'],
    ['area', '地区'],
    ['industry', '行业'],
    ['deviceNumber', '设备数量', [v => !isNaN(v) && v > 0 || '请输入设备数量']],
    ['contact', '联系人'],
    ['phone', '联系手机号'],
    ['email', '邮箱']
  ]
}
