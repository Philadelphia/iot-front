export default {
  title: '设备故障统计'
}

export const mixins = [
  {
    data () {
      return {
        timeOptions: [
          '最近一小时',
          '最近一天',
          '最近一周',
          '最近一月',
          '自定义时间'
        ].map((label, i) => ({
          label,
          value: i + 1
        }))
      }
    }
  }
]
