export default {
  data () {
    return {
      cardStatusOptions: [
        '可测试',
        '可激活',
        '已激活',
        '正常',
        '库存',
        '其他'
      ].map((label, i) => ({
        label,
        value: i
      }))
    }
  }
}
