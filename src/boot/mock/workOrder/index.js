export default [
  [
    'breakdownList', {
      'list|10': [{
        no: 'gz_@date("yyyyMMdd")',
        level: '@pick(["高", "低", "中"])',
        deviceId: 'x@natural(100, 999)',
        deviceName: 'Practic-cnc@natural(1, 10)',
        breakdownName: '@pick(["离线告警", "A相电压", "电流B", "电流C相"])',
        breakdownType: '@pick(["离线告警", "A相电压", "电流B", "电流C相"])',
        createTime: '@date("yyyy-MM-dd HH:mm:ss")',
        handleTime: '@date("yyyy-MM-dd HH:mm:ss")',
        status: '@pick(["已处理", "未处理", "处理中"])',
        handlerName: '@cname'
      }]
    }
  ]
]
