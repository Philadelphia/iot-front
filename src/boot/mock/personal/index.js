export default [
  [
    'myNotDoneMissionList',
    {
      'list|10': [{
        'id|+1': 1,
        workOrder: 'gz_@date("YYYYMMDD")@natural(10000, 100000)',
        type: '@pick([1, 2])',
        status: '@natural(1, 3)',
        breadDownName: '@pick(["电流A相", "相电压C", "电池电流", "A相电压"])',
        createTime: '@datetime'
      }]
    }
  ]
]
