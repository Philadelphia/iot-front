import { uid } from 'quasar'

const permissions = [
  '/',
  '/card/list',
  '/card/status',
  '/client/info',
  '/device/device-info',
  '/device/offline-warning',
  '/device/opration-log',
  '/device/param-setting',
  '/device/realtime-monitor',
  '/device/remote-control',
  '/device/warning-manage',
  '/report/faultStatistics',
  '/report/runInfo',
  '/system/roleManage',
  '/system/userManage',
  '/work-order/breakdown',
  '/work-order/history',
  '/work-order/repairs'
].map(path => ({ path }))

export default [
  ['login', {
    'username': 'Mock用户',
    'companyId': '@natural(0, 1000)',
    'roles': ['角色1', '角色2'],
    'token': uid(),
    permissions
  }]
]
