const company = '@pick(["中国联通", "亚信科技"])'

export default [
  [
    'list',
    {
      'list|10': [{
        'id|+1': 1,
        iccid: '@natural',
        imsi: '@natural',
        msisdn: '@natural',
        attributionCompany: company,
        purchaseCardCompany: company,
        productName: '@pick(["产品名称1", "产品名称2", "产品名称3"])',
        saleStatus: '@pick([0, 1])',
        cardStatus: '@natural(0, 5)',
        flowUsage: '@natural(2048, 20480)',
        flowQuota: '@natural(2048, 20480)'
      }],
      'totalRows': '@natural(100, 200)'
    }
  ],
  [
    'statusList',
    {
      'list|10': [{
        'id|+1': 1,
        virtualAccountId: '@natural',
        iccid: '@natural',
        status: '@natural(0, 5)',
        msisdn: '@natural',
        imsi: '@natural',
        deviceId: '@natural',
        operatorId: /^\d{3}[A-Z]{3}\d{6}$/,
        firstActivateDate: '@datetime',
        payStatus: '@pick(0, 1)',
        communicationPlan: '通信计划@pick(1, 2, 3, 4, 5, 6, 7, 8)',
        hasOpen: '@boolean',
        isStop: '@boolean'
      }]
    }
  ],
  [
    'usageHistory',
    {
      'list|10': [{
        'id|+1': 1,
        iccid: '@natural',
        operatorId: /^\d{3}[A-Z]{3}\d{6}$/,
        billDate: '@date',
        accumulateUsage: '@natural(9999, 99999999)',
        proportion: '@natural(1, 99)%'
      }]
    }
  ],
  [
    'usageRealtime',
    {
      'list|10': [{
        'id|+1': 1,
        virtualAccountId: '@natural',
        iccid: '@natural',
        operatorId: /^\d{3}[A-Z]{3}\d{6}$/,
        msisdn: '@natural',
        status: '@natural(0, 5)',
        usage: '@natural(9999, 99999)',
        quota: '@natural(99999, 999999)',
        textMessageUsage: '@natural(99, 999)',
        textMessageQuota: '@natural(999, 9999)',
        voiceUsage: '@natural(9999, 99999)',
        voiceQuota: '@natural(99999, 999999)'
      }],
      totalRows: '@natural(100, 200)'
    }
  ],
  [
    'consume',
    {
      'list|10': [{
        'id|+1': 1,
        virtualAccountId: '@natural',
        iccid: '@natural',
        statisticsDate: '@date',
        theDayUsage: '@natural(999, 99999)',
        theDayUsageAccumulate: '@natural(99999, 9999999)',
        theDayTextMessageUsage: '@natural(999, 99999)',
        theDayTextMessageUsageAccumulate: '@natural(999, 99999)',
        theDayVoiceUsage: '@natural(999, 99999)',
        theDayVoiceUsageAccumulate: '@natural(999, 99999)'
      }]
    }
  ],
  [
    'businessQuery',
    {
      'list|10': [{
        'id|+1': 1,
        name: '@pick(["业务1", "业务2", "业务3", "业务4", "业务5"])',
        num: /^[A-Za-z]{3}\d{7,10}$/
      }]
    }
  ]
]
