export default [
  [
    'runInfoList',
    {
      'list|10': [{
        deviceId: 'x@natural(100, 999)',
        clientName: '@cname',
        deviceName: 'Practic-cnc@natural(1, 10)',
        status: '@natural(0, 1)'
      }]
    }
  ],
  [
    'singleStatitic',
    {
      'list|10': [{
        deviceName: 'Practic-cnc@natural(1, 10)',
        clientName: '@cname',
        gatewayId: '@natural(10000000, 10000000000)',
        status: '@natural(0, 1)',
        warningTimes: '@natural(1, 10)',
        breakdownTimes: '@natural(1, 10)'
      }]
    }
  ],
  [
    'breakdownTimeList',
    {
      'list|10': [{
        start: '@datetime',
        end: '@datetime'
      }]
    }
  ],
  [
    'onlineTimeList',
    {
      'list|10': [{
        deviceId: '432',
        date: '@date',
        deviceName: 'Pratic-cnc3036',
        clientName: '亚信科技',
        onlineDuration: '@natural(1, 24)小时@natural(1, 60)分'
      }],
      totalRows: 200
    }
  ]
]
