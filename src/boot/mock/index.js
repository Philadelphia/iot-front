const Mock = require('mockjs')

const { MOCK_BASE } = process.env

// 根据从层级自动组装mock文件夹下的模块
export default () => {
  // if (USE_MOCK) {
  const requireMocks = require.context(
    './',
    true,
    /[a-z]\/index\.js$/
  )
  requireMocks.keys().forEach(mockFilePath => {
    let moduleBase = mockFilePath.split('/').slice(-2)[0]
    requireMocks(mockFilePath).default.forEach(([subUrl, message]) => {
      Mock.mock(`${MOCK_BASE}${moduleBase}/${subUrl}`, 'post', {
        code: 8888,
        message
      })
    })
  })
  // }
}
