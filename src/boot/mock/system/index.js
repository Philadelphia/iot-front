export default [
  [
    'userList',
    {
      'list|10': [{
        companyName: '亚信科技',
        nickname: '@cname',
        username: '@name',
        password: '@word',
        telephone: /[0-9]{4}-[0-9]{3}-[0-9]{3}/,
        phone: /1([3-9])[0-9][0-9]{4}[0-9]{4}/
      }]
    }
  ],
  [
    'roleList',
    {
      'list|10': [{
        name: '@cname',
        englishName: '@last',
        company: '亚信科技',
        dataRange: '@word'
      }]
    }
  ]
]
