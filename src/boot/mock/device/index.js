import { Random } from 'mockjs'
const typeReg = /[A-Z][a-z]{3,7}_[a-z]{3}[0-9]{1,3}/,
  paramName = /[a-z][0-9]_[a-z][0-9]_tag[0-9]/,
  phone = /1([3-9])[0-9][0-9]{4}[0-9]{4}/,
  deviceDescription = '@pick(["无功功率", "有功功率", "电流C", "电流B", "电流A", "电流D", "功率因数"])',
  deviceType = '@pick(["电机", "直流", "交流", "工业", "商业"])'
export default [
  [
    'list',
    {
      'list|10': [{
        status: '@natural(1, 2)',
        softwareVersion: '@natural(0,10).@natural(0,10)',
        'number|1-99999999': 1,
        machineType: typeReg,
        province: '@province',
        machineSerialNumber: '@natural',
        name: typeReg,
        clientName: '@cname',
        type: deviceType,
        'id|+1': 1,
        modelNumber: '@natural',
        routerIMEI: '@natural(1000, 9999999)@word',
        SIMCardNumber: '@natural',
        signalStrength: Random.cword('强中弱'),
        acceptDate: '@date',
        releaseDate: '@date',
        installDate: '@date',
        encoding: '@word',
        installerName: '@word',
        installerEncoding: '@word',
        primaryElectricElementModelNumber: '@natural',
        primaryElectricElementInstallPosition: '@word',
        nameplateInfo: '@word',
        city: '@word',
        gatewayId: '@natural',
        onlineTime: '@date',
        runningDuration: '@natural(1, 100)天@natural(1, 24)时@natural(1, 60)秒'
      }]
    }
  ],
  [
    'insert', 'Insert success'
  ],
  [
    'delete', 'Delete success'
  ],
  [
    'listClients',
    {
      'list|10': [{
        'id|+1': 1,
        deviceNumber: '@natural(100, 1000)',
        name: '@cname',
        isRemoteLocate: '@boolean',
        tel: '@natural(100, 999)-@natural(100000, 999999)',
        clientAddress: '@word',
        platformName: '@word',
        industry: '@word',
        province: '@province',
        address: '@province()@city()',
        city: '@city',
        area: '@region',
        account: '@word',
        password: '@word',
        contact: '@cname',
        email: '@word(3,7)@asiainfo.com',
        phone
      }]
    }
  ],
  [
    'listUnaddedPermissions',
    {
      'list|5': [{
        'id|+1': 1,
        name: '@word'
      }]
    }
  ],
  [
    'listAddedPermissions', {
      'list|5': [{
        id: '@natural(100, 1000)',
        name: '@word'
      }]
    }
  ],
  [
    'deviceParams', {
      'list|10': [{
        id: '@natural(100, 1000)',
        paramName,
        desc: deviceDescription,
        type: typeReg,
        dataType: '@word',
        clientName: '@cname'
      }]
    }
  ],
  [
    'getDeviceParamsByDeviceId', {
      'list|10': [{
        id: '@natural(100, 1000)',
        deviceId: '',
        paramName: /[a-z][0-9]_[a-z][0-9]_tag[0-9]/,
        desc: deviceDescription,
        type: typeReg,
        order: '@natural(1, 1000)',
        frontEndShow: '@boolean',
        emphasisShow: '@boolean',
        mobileShow: '@boolean'
      }]
    }
  ],
  [
    'realtimeMonitorList', {
      'list|12': [{
        name: typeReg,
        online: '@boolean',
        current: '@natural(10, 100)',
        voltage: '@natural(10, 100)',
        voltage2: '@natural(10, 100)',
        frequency: '@natural(10, 100)',
        status: '@natural(1000, 10000)'
      }]
    }
  ],
  [
    'realtimeWarningList', {
      'list|10': [{
        deviceName: typeReg,
        clientName: '@cname',
        name: '离线警告',
        level: '@pick(["高", "中", "低"])',
        type: '@pick(["类型一", "类型二", "类型三"])',
        content: '亚信科技的设备@natural(100, 999)已离线，离线时间为@date("yyyy-MM-dd HH:mm:ss")',
        time: '@date(yyyy-MM-dd HH:mm:ss)',
        triggerWorkOrder: '@boolean'
      }]
    }
  ],
  [
    'warningRules', {
      'list|10': [{
        deviceName: typeReg,
        ruleName: '@pick(["较小", "较大", "电流下降", "电压", "电流"])',
        paramNumber: '@natural(1, 10)',
        triggerWorkOrder: '@boolean',
        noticeMethod: '@pick(["邮件通知", "短信通知"])'
      }]
    }
  ],
  [
    'remoteDeviceList', {
      'list|10': [{
        deviceName: typeReg,
        clientName: '@cname',
        deviceType,
        deviceStatus: '@natural(1,2)',
        deviceId: '@natural'
      }]
    }
  ],
  [
    'switchList', {
      'list|10': [{
        deviceName: typeReg,
        paramNumber: '@natural(1, 10)'
      }]
    }
  ],
  [
    'oprationLogs', {
      'list|10': [{
        deviceName: typeReg,
        clientName: '@cname',
        deviceType: typeReg,
        currentWarning: '离线警告',
        operateTime: '@date(yyyy-MM-dd)',
        operator: '@cname',
        content: paramName,
        result: '@natural(0, 1)'
      }]
    }
  ],
  [
    'offlineWarningList', {
      'list|10': [{
        deviceName: typeReg,
        phone,
        time: '@natural(1, 100)分钟'
      }]
    }
  ]
]
