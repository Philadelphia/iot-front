import { Notify, Dialog } from 'quasar'

const showMsg = (message, color, icon) => {
  Notify.create({
    color,
    textColor: 'white',
    message,
    position: 'center',
    icon,
    timeout: 2000,
    actions: [{ icon: 'close', color: 'white' }]
  })
}

const notify = {
  success: message => showMsg(message, 'green-5', 'done'),
  error: message => showMsg(message, 'red-5', 'error'),
  warning: message => showMsg(message, 'orange-5', 'warning'),
  info: message => showMsg(message, 'blue-5', 'info'),
  confirm: message => new Promise(resolve => {
    Dialog.create({
      title: '确认',
      message,
      color: 'primary',
      cancel: true,
      persistent: true,
      dark: true
    }).onOk(() => {
      resolve()
    }).onCancel(() => {})
  })
}

export default ({ Vue }) => {
  Vue.prototype.$uc = {
    notify
  }
}

export {
  notify
}
