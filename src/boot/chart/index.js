import Chart from 'chart.js'

export default () => {
  Chart.plugins.register({
    id: 'DoughnutCenterText',
    beforeDraw ({
      ctx,
      options
    }) {
      const { width, height } = ctx.canvas
      const pluginConfig = options.plugins.DoughnutCenterText
      const { textColor, textContent } = pluginConfig
      ctx.font = '24px serif'
      ctx.textAlign = 'center'
      ctx.textBaseline = 'middle'
      ctx.fillStyle = textColor
      ctx.fillText(textContent, width / 2, height / 2);
    }
  })
}
