export default ({ Vue }) => {
  [
    'TabelHeaderQueryForm',
    'FormItem',
    'DatePicker',
    'CommonTable',
    'CommonDialog',
    'CommonForm'
  ].forEach(name => {
    Vue.component(name, () => import(`components/${name}`))
  })
}
