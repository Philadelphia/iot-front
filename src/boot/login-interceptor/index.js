import { SessionStorage } from 'quasar'

const TOKEN_KEY = 'IOT_LOGIN_TOKEN'
const COMPANYID_KEY = 'IOT_COMPANY_ID'
const USERNAME_KEY = 'IOT_USERNAME'
const PERMISSION_ROUTE_KEY = 'IOT_PERMISSION_ROUTE'
const USER_ID_KEY = 'IOT_USER_ID'

const hasToken = () => SessionStorage.has(TOKEN_KEY)

const getPermissionRoutes = () => SessionStorage.getItem(PERMISSION_ROUTE_KEY)

const logout = () => {
  SessionStorage.remove(TOKEN_KEY)
  SessionStorage.remove(COMPANYID_KEY)
  SessionStorage.remove(USERNAME_KEY)
  SessionStorage.remove(PERMISSION_ROUTE_KEY)
  SessionStorage.remove(USER_ID_KEY)
}

export default ({ Vue, store, router }) => {
  router.beforeEach((to, from, next) => {
    let { needLoign, needPermission } = to.meta
    if (needLoign && !hasToken()) {
      store.commit('login/set', {
        k: 'targetRoute',
        v: to.fullPath
      })
      next('/login')
      return
    }
    if (needPermission && !getPermissionRoutes().some(({ path }) => path === to.path)) {
      Vue.prototype.$uc.notify.error('没有权限！请重新登录!')
      logout()
      next('/login')
      return
    }
    next()
  })
}

export const getToken = () => SessionStorage.getItem(TOKEN_KEY)
export const getCompanyId = () => SessionStorage.getItem(COMPANYID_KEY)
export const getUsername = () => SessionStorage.getItem(USERNAME_KEY)
export const getUserId = () => SessionStorage.getItem(USER_ID_KEY)
export const setToken = ({
  token,
  companyId,
  username,
  permissions,
  userId
}) => {
  SessionStorage.set(TOKEN_KEY, token)
  SessionStorage.set(COMPANYID_KEY, companyId)
  SessionStorage.set(USERNAME_KEY, username)
  SessionStorage.set(PERMISSION_ROUTE_KEY, permissions)
  SessionStorage.set(USER_ID_KEY, userId)
}

export {
  logout,
  hasToken,
  getPermissionRoutes
}
