export default [
  ['list'],
  ['update'],
  ['save'],
  ['delete'],
  ['queryList'],
  ['getUSDRole'],
  ['getSDRole'],
  ['addRole'],
  ['removeRole'],
  ['getAll']
]
