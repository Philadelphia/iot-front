import axios from 'axios'
import { getToken, hasToken, getCompanyId, getUsername, getUserId } from 'boot/login-interceptor'

const { AXIOS_BASE, MOCK_BASE } = process.env

const API_INSTANCE = axios.create({
  baseURL: AXIOS_BASE,
  timeout: 10000
});

const MOCK_INSTANCE = axios.create({
  baseURL: MOCK_BASE,
  timeout: 5000
})

export default ({ Vue }) => {
  const reqInterceptor = (config) => {
    if (hasToken()) {
      config.headers['token'] = getToken()
      config.headers['companyId'] = getCompanyId()
      config.headers['username'] = getUsername()
      config.headers['userId'] = getUserId()
    }
    return config
  }

  API_INSTANCE.interceptors.request.use(reqInterceptor)

  const importAll = (r, code, useMessage = true, codeField = 'code', axiosInstance) => {
    return r.keys().reduce((apiObj, apiPath) => {
      let moduleBase = apiPath.split('/').slice(-2)[0]
      apiObj[moduleBase] = r(apiPath).default.reduce((moduleObj, [subUrl]) => {
        moduleObj[subUrl] = (d = {}, params = {}) => new Promise(resolve => {
          axiosInstance.post(`${moduleBase}/${subUrl}`, d, { params }).then(({ data }) => {
            if (data[codeField] !== code) {
              Vue.prototype.$uc.notify.error(data.message)
              return
            }
            resolve(useMessage ? data.message : data)
          })
        })
        return moduleObj
      }, {})
      return apiObj
    }, {})
  }

  Vue.prototype.$mock = importAll(require.context(
    '../mock/',
    true,
    /[a-z]\/index\.js$/
  ), 8888, true, 'code', MOCK_INSTANCE)

  Vue.prototype.$api = importAll(require.context(
    './api/',
    true,
    /index\.js$/
  ), '200', false, 'statusCode', API_INSTANCE)

  Vue.prototype.$axios = (u, d, params) => new Promise((resolve, reject) => {
    API_INSTANCE.post(u, d, { params }).then(({ data }) => {
      let { message, statusCode } = data
      if (statusCode !== '200') {
        Vue.prototype.$uc.notify.error(message)
        return
      }
      resolve(data)
    }).catch(({ response: { data: { message } } }) => {
      Vue.prototype.$uc.notify.error(message)
      reject(message)
    })
  })
}
