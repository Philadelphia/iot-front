// 通过页面目录结构自动解析路由
const requirePages = require.context(
  '../../pages',
  true,
  /config\.js/
)

const children = requirePages.keys().reduce((routes, parentConfig) => {
  let arr = parentConfig.split('/')
  if (arr.length > 3) {
    return routes
  }
  let parentUrl = arr[1]
  let { title, exclude = false, icon, needLoign = true, hidden = false, needPermission = true } = requirePages(parentConfig).default
  let parentTitle = title
  if (!exclude) {
    let children = requirePages.keys().filter(fp => {
      let arr = fp.split('/')
      return arr[1] === parentUrl && arr.length > 3
    }).map(childConfig => {
      let subUrl = childConfig.split('/')[2]
      let { title, needLoign = true, hidden = false, needPermission = true } = requirePages(childConfig).default
      return {
        path: subUrl,
        component: () => import(`pages/${parentUrl}/${subUrl}`),
        meta: {
          parentTitle,
          title,
          needLoign,
          hidden,
          needPermission
        }
      }
    })
    routes.push({
      path: parentUrl,
      component: () => import('layouts/MainPage.vue'),
      meta: {
        title: parentTitle,
        icon,
        hidden,
        needLoign,
        needPermission
      },
      children
    })
  }
  return routes
}, [])

children.unshift({
  path: '',
  component: () => import('pages/Index'),
  meta: {
    title: '首页',
    needLoign: true
  }
})

const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children
  },
  {
    path: '/login',
    component: () => import('pages/login'),
    meta: {
      needLoign: false
    }
  }, {
    path: '/big-screen',
    component: () => import('pages/big-screen'),
    meta: {
      title: '大屏展示',
      needPermission: false
    }
  }
]

// Always leave this as last one

if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

const filterRoute = r => {
  if (r.children && r.children.length > 0) {
    r.children = r.children.filter(filterRoute)
  }
  return !r.meta || !r.meta.hidden
}

export default routes.filter(filterRoute)
