import {
  Loading
} from 'quasar'
import XLSX from 'xlsx'
import { notify } from 'boot/unicom-tools'

const getHeaderRow = (sheet) => {
  const headers = []
  const range = XLSX.utils.decode_range(sheet['!ref'])
  let C
  const R = range.s.r
  /* start in the first row */
  for (C = range.s.c; C <= range.e.c; ++C) { /* walk every column in the range */
    const cell = sheet[XLSX.utils.encode_cell({ c: C, r: R })]
    /* find the cell in the first row */
    let hdr = 'UNKNOWN ' + C // <-- replace with your desired default
    if (cell && cell.t) hdr = XLSX.utils.format_cell(cell)
    headers.push(hdr)
  }
  return headers
}

const readerData = (rawFile) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = e => {
      const data = e.target.result
      const workbook = XLSX.read(data, { type: 'array' })
      const firstSheetName = workbook.SheetNames[0]
      const worksheet = workbook.Sheets[firstSheetName]
      const header = getHeaderRow(worksheet)
      const results = XLSX.utils.sheet_to_json(worksheet)
      resolve({ header, results })
    }
    reader.readAsArrayBuffer(rawFile)
  })
}

/**
 * [exportExcel 导出表格数据]
 * @param  {[type]} {}     [无用参数]
 * @param  {[type]} headerInFieldNameAndTitleTowDimensionArray [表格头二维数组, [[字段名称, 字段标题], [字段名称, 字段标题]]]
 * @param  {[type]} dataObjArr [表格数据对象数组, [{每个header字段名称：字段值}]]
 * @param  {[type]} filename  [文件名称]
 * @return {[type]} bookType  [文件类型]
 */
export const exportExcel = (unused, { headerInFieldNameAndTitleTowDimensionArray, dataObjArr, filename, bookType = 'xlsx' }) => {
  Loading.show({
    message: '导出中...',
    backgroundColor: 'rgba(0, 0, 0, .8)'
  })
  setTimeout(() => {
    import('src/utils/Export2Excel').then(excel => {
      excel.export_json_to_excel({
        header: headerInFieldNameAndTitleTowDimensionArray.map(([fieldName, label]) => label),
        data: dataObjArr.map(rowObj => {
          const rowArr = []
          headerInFieldNameAndTitleTowDimensionArray.forEach(([f, l, formatter = v => v]) => {
            rowArr.push(formatter(rowObj[f]))
          })
          return rowArr
        }),
        filename,
        autoWidth: true,
        bookType
      })
      Loading.hide()
    })
  }, 1000)
}

/**
 *
 * @param uploadExcelComponentInstance uploadExcel组件实例
 * @param fileObj 文件对象
 * @param tableColumnSchema 表格描述二维数组 [[字段名, 字段显示名]]
 */
export const uploadExcel = (_, { fileObj, tableColumnSchema }) => {
  Loading.show({
    message: 'Excel解析中...',
    backgroundColor: 'rgba(0, 0, 0, .8)'
  })
  return new Promise((resolve) => {
    let hasError = false
    setTimeout(() => {
      return readerData(fileObj).then(({ results }) => {
        const labelToPropMap = new Map(tableColumnSchema.map(([f, l, formatter = v => v]) => [l, [f, formatter]]))
        const analysisRes = results.map(row => Object.keys(row).reduce((obj, label) => {
          const [field, formatter] = labelToPropMap.get(label) || []
          if (!field || !formatter) {
            hasError = true
            return obj
          }
          obj[field] = formatter(row[label])
          return obj
        }, {}))
        Loading.hide()
        if (!hasError) {
          resolve(analysisRes)
          return
        }
        notify.error('解析Excel出错！')
      })
    }, 1000)
  })
}
